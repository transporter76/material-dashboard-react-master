import React, { useState, useEffect } from "react";
import {
  XYPlot,
  XAxis,
  YAxis,
  ChartLabel,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
  MarkSeries
} from "react-vis";
import "react-vis/dist/style.css";
import "date-fns";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grow from "@material-ui/core/Grow";
import axios from "axios";
import moment from "moment";
import MUIDataTable from "mui-datatables";

//Form styles
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 240
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
}));

//Function starts here
function App() {
  //Setting ivData, default value takes BANKNIFTY with 28 Nov Expiry
  const [ivData, setIVData] = useState([]);

  const postData = () => {
    axios
      .post(
        "http://localhost:9000/getData/IVChart?Stock=" +
          symbol +
          "&ExpiryDate=" +
          edate.toISOString() +
          "&SnapshotDate="
      )
      .then(res => setIVData(res.data.ivdata));
  };

  // var dict = []
  // {ivData.map(date => (dict.push({
  //x: moment(date.x).format('MMM Do YY'),
  //y: date.y
  //})))}

  //Setting styles
  const classes = useStyles();

  //Setting FNO list
  const [symbol, setFo] = React.useState("BANKNIFTY");

  //Handling change when FNO stock selected from dropdown
  const handleChange = event => {
    setFo(event.target.value);
  };

  // Setting the expiry date and handling changes
  const [edate, setSelectedExDate] = React.useState(
    new Date("2019-12-05T21:11:54")
  );
  const handleExDateChange = date => {
    setSelectedExDate(date);
  };

  useEffect(() => {
    function defaultChart() {
      axios
        .post(
          "http://localhost:9000/getData/IVChart?Stock=" +
            symbol +
            "&ExpiryDate=" +
            edate.toISOString() +
            "&SnapshotDate="
        )
        .then(res => setIVData(res.data.ivdata));
    }
    defaultChart();
  }, [symbol, edate]);

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  //FNO list
  const fno = [
    "BANKNIFTY",
    "NIFTY",
    "NIFTYIT",
    "ACC",
    "ADANIENT",
    "ADANIPOWER",
    "AMARAJABAT",
    "APOLLOHOSP",
    "APOLLOTYRE",
    "AUROPHARMA",
    "ADANIPORTS",
    "BAJAJ-AUTO",
    "BAJAJFINSV",
    "BAJFINANCE",
    "BANKBARODA",
    "BATAINDIA",
    "BEL",
    "BHARTIARTL",
    "BIOCON",
    "BOSCHLTD",
    "BPCL",
    "CADILAHC",
    "CESC",
    "CHOLAFIN",
    "COALINDIA",
    "COLPAL",
    "CONCOR",
    "CUMMINSIND",
    "BERGEPAINT",
    "DABUR",
    "DLF",
    "EICHERMOT",
    "EQUITAS",
    "ESCORTS",
    "FEDERALBNK",
    "GAIL",
    "GODREJCP",
    "GRASIM",
    "HAVELLS",
    "HCLTECH",
    "HDFC",
    "HEXAWARE",
    "HINDALCO",
    "HINDPETRO",
    "HINDUNILVR",
    "IBULHSGFIN",
    "ICICIBANK",
    "IDFCFIRSTB",
    "IGL",
    "INDIGO",
    "INDUSINDBK",
    "INFRATEL",
    "ITC",
    "JSWSTEEL",
    "JUBLFOOD",
    "JUSTDIAL",
    "KOTAKBANK",
    "L&TFH",
    "LICHSGFIN",
    "LT",
    "MCDOWELL-N",
    "MFSL",
    "MRF",
    "MUTHOOTFIN",
    "NATIONALUM",
    "NBCC",
    "ICICIPRULI",
    "NESTLEIND",
    "NIITTECH",
    "NMDC",
    "ONGC",
    "PAGEIND",
    "PEL",
    "PFC",
    "POWERGRID",
    "PVR",
    "RAMCOCEM",
    "RBLBANK",
    "RECLTD",
    "SIEMENS",
    "SUNPHARMA",
    "TATACHEM",
    "TATAGLOBAL",
    "TATAPOWER",
    "TCS",
    "TORNTPHARM",
    "TORNTPOWER",
    "UBL",
    "UJJIVAN",
    "VEDL",
    "VOLTAS",
    "WIPRO",
    "YESBANK",
    "ZEEL",
    "LUPIN",
    "MANAPPURAM",
    "NCC",
    "AMBUJACEM",
    "ASIANPAINT",
    "AXISBANK",
    "BALKRISIND",
    "BANKINDIA",
    "BHEL",
    "CANBK",
    "UNIONBANK",
    "CASTROLIND",
    "CENTURYTEX",
    "DISHTV",
    "DIVISLAB",
    "EXIDEIND",
    "GLENMARK",
    "IOC",
    "M&M",
    "M&MFIN",
    "MARICO",
    "MARUTI",
    "MGL",
    "MINDTREE",
    "MOTHERSUMI",
    "NTPC",
    "OIL",
    "PIDILITIND",
    "PNB",
    "RELIANCE",
    "SAIL",
    "SBIN",
    "SHREECEM",
    "SRF",
    "SUNTV",
    "TATAMTRDVR",
    "TATASTEEL",
    "TECHM",
    "TITAN",
    "TVSMOTOR",
    "ULTRACEMCO",
    "UPL",
    "HDFCBANK",
    "ASHOKLEY",
    "BRITANNIA",
    "DRREDDY",
    "INFY",
    "SRTRANSFIN",
    "TATAELXSI",
    "TATAMOTORS",
    "BHARATFORG",
    "CIPLA",
    "HEROMOTOCO",
    "GMRINFRA",
    "IDEA",
    "JINDALSTEL",
    "PETRONET"
  ];

  const dictcolumns = [
    {
      name: "x",
      label: "Date",
      options: {
        filter: true,
        sort: true,
        display: true
      }
    },
    {
      name: "y",
      label: "Implied Volatility",
      options: {
        filter: true,
        sort: true,
        display: true
      }
    }
  ];

  return (
    <div className="App">
      <Grid container justify="space-around">
        <FormControl
          variant="outlined"
          className={classes.formControl}
          inputProps={{ name: "symbol" }}
        >
          <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
            Select Underlying
          </InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={symbol}
            onChange={handleChange}
            labelWidth={labelWidth}
            inputProps={{ name: "symbol" }}
          >
            {fno.map((fno, index) => (
              <MenuItem
                inputProps={{ name: "symbol" }}
                key={index}
                value={fno}
                primaryText={fno}
                onChange={handleChange}
              >
                {fno}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            format="yyyy-MM-dd"
            margin="normal"
            variant="inline"
            name="edate"
            id="date-picker-inline"
            label="Select Expiry Date"
            value={edate}
            onChange={handleExDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date"
            }}
          />
        </MuiPickersUtilsProvider>
        <p>
          <FormControl variant="outlined" className={classes.formControl}>
            <Button
              variant="contained"
              className={classes.button}
              type="submit"
              value="submit"
              onClick={postData}
            >
              Submit
            </Button>
          </FormControl>
        </p>
      </Grid>

      <XYPlot width={1300} height={300} xType="ordinal">
        <HorizontalGridLines />
        <VerticalGridLines />
        <XAxis tickFormat={value => moment(value).format("DD/MM/YYYY")} />
        <YAxis />
        <MarkSeries
          className="mark-series-example"
          sizeRange={[5, 15]}
          data={ivData}
        />

        <ChartLabel
          text="Date"
          className="alt-x-label"
          includeMargin={false}
          xPercent={0.0}
          yPercent={1.01}
        />

        <ChartLabel
          text="Implied Volatility"
          className="alt-y-label"
          includeMargin={false}
          xPercent={0.0}
          yPercent={0.06}
          style={{
            transform: "rotate(-90)",
            textAnchor: "end"
          }}
        />
        <LineSeries className="first-series" data={null} />
        <LineSeries className="second-series" data={ivData} />
      </XYPlot>
      <div style={{ height: "100%", width: "100%" }}>
        <Grow in={true}>
          <MUIDataTable
            title={" Option IV Data "}
            data={ivData}
            columns={dictcolumns}
          />
        </Grow>
      </div>
    </div>
  );
}

export default App;
