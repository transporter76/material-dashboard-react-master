import "date-fns";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import React, { useState, useEffect } from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import MUIDataTable from "mui-datatables";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 240
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
}));

function OptChain() {
  const [stockData, setOptiondata] = useState([]);

  const postData = () => {
    axios
      .post(
        "http://localhost:9000/getData/HistoricalOptionChain?Stock=" +
          symbol +
          "&ExpiryDate=" +
          edate.toISOString() +
          "&SnapshotDate=" +
          sdate.toISOString()
      )
      .then(res => setOptiondata(res.data.Stockdata));
  };

  //Setting styles
  const classes = useStyles();

  //Setting FNO list
  const [symbol, setFo] = React.useState("BANKNIFTY");

  //Handling change when FNO stock selected from dropdown
  const handleChange = event => {
    setFo(event.target.value);
  };

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  // The first commit of Material-UI
  const [edate, setSelectedExDate] = React.useState(
    new Date("2019-12-05T21:11:54")
  );

  const handleExDateChange = date => {
    setSelectedExDate(date);
  };

  const [sdate, setSelectedDate] = React.useState(
    new Date("2019-12-03T21:11:54")
  );

  const handleDateChange = date => {
    setSelectedDate(date);
  };

  useEffect(() => {
    function defaultData() {
      axios
        .post(
          "http://localhost:9000/getData/HistoricalOptionChain?Stock=" +
            symbol +
            "&ExpiryDate=" +
            edate.toISOString() +
            "&SnapshotDate=" +
            sdate.toISOString()
        )
        .then(res => setOptiondata(res.data.Stockdata));
    }
    defaultData();
  }, [symbol, edate, sdate]);

  //FNO list
  const fno = [
    "BANKNIFTY",
    "NIFTY",
    "NIFTYIT",
    "ACC",
    "ADANIENT",
    "ADANIPOWER",
    "AMARAJABAT",
    "APOLLOHOSP",
    "APOLLOTYRE",
    "AUROPHARMA",
    "ADANIPORTS",
    "BAJAJ-AUTO",
    "BAJAJFINSV",
    "BAJFINANCE",
    "BANKBARODA",
    "BATAINDIA",
    "BEL",
    "BHARTIARTL",
    "BIOCON",
    "BOSCHLTD",
    "BPCL",
    "CADILAHC",
    "CESC",
    "CHOLAFIN",
    "COALINDIA",
    "COLPAL",
    "CONCOR",
    "CUMMINSIND",
    "BERGEPAINT",
    "DABUR",
    "DLF",
    "EICHERMOT",
    "EQUITAS",
    "ESCORTS",
    "FEDERALBNK",
    "GAIL",
    "GODREJCP",
    "GRASIM",
    "HAVELLS",
    "HCLTECH",
    "HDFC",
    "HEXAWARE",
    "HINDALCO",
    "HINDPETRO",
    "HINDUNILVR",
    "IBULHSGFIN",
    "ICICIBANK",
    "IDFCFIRSTB",
    "IGL",
    "INDIGO",
    "INDUSINDBK",
    "INFRATEL",
    "ITC",
    "JSWSTEEL",
    "JUBLFOOD",
    "JUSTDIAL",
    "KOTAKBANK",
    "L&TFH",
    "LICHSGFIN",
    "LT",
    "MCDOWELL-N",
    "MFSL",
    "MRF",
    "MUTHOOTFIN",
    "NATIONALUM",
    "NBCC",
    "ICICIPRULI",
    "NESTLEIND",
    "NIITTECH",
    "NMDC",
    "ONGC",
    "PAGEIND",
    "PEL",
    "PFC",
    "POWERGRID",
    "PVR",
    "RAMCOCEM",
    "RBLBANK",
    "RECLTD",
    "SIEMENS",
    "SUNPHARMA",
    "TATACHEM",
    "TATAGLOBAL",
    "TATAPOWER",
    "TCS",
    "TORNTPHARM",
    "TORNTPOWER",
    "UBL",
    "UJJIVAN",
    "VEDL",
    "VOLTAS",
    "WIPRO",
    "YESBANK",
    "ZEEL",
    "LUPIN",
    "MANAPPURAM",
    "NCC",
    "AMBUJACEM",
    "ASIANPAINT",
    "AXISBANK",
    "BALKRISIND",
    "BANKINDIA",
    "BHEL",
    "CANBK",
    "UNIONBANK",
    "CASTROLIND",
    "CENTURYTEX",
    "DISHTV",
    "DIVISLAB",
    "EXIDEIND",
    "GLENMARK",
    "IOC",
    "M&M",
    "M&MFIN",
    "MARICO",
    "MARUTI",
    "MGL",
    "MINDTREE",
    "MOTHERSUMI",
    "NTPC",
    "OIL",
    "PIDILITIND",
    "PNB",
    "RELIANCE",
    "SAIL",
    "SBIN",
    "SHREECEM",
    "SRF",
    "SUNTV",
    "TATAMTRDVR",
    "TATASTEEL",
    "TECHM",
    "TITAN",
    "TVSMOTOR",
    "ULTRACEMCO",
    "UPL",
    "HDFCBANK",
    "ASHOKLEY",
    "BRITANNIA",
    "DRREDDY",
    "INFY",
    "SRTRANSFIN",
    "TATAELXSI",
    "TATAMOTORS",
    "BHARATFORG",
    "CIPLA",
    "HEROMOTOCO",
    "GMRINFRA",
    "IDEA",
    "JINDALSTEL",
    "PETRONET"
  ];

  //Mui tables here
  const columns = [
    {
      name: "date",
      label: "Date",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "symbol",
      label: "Symbol",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "expiry",
      label: "Expiry",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "underlying",
      label: "Underlying",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "change_oi_call",
      label: "Change in OI Call",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "oi_call",
      label: "OI Call",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "premium_turnover_call",
      label: "Premium Turnover Call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "turnover_call",
      label: "Turnover Call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "number_of_contracts_call",
      label: "number_of_contracts_call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "open_call",
      label: "open_call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "high_call",
      label: "high_call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "low_call",
      label: "low_call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "close_call",
      label: "close_call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "vega_call",
      label: "Vega Call",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "theta_call",
      label: "Theta Call",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "delta_call",
      label: "Delta Call",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "iv_call",
      label: "IV Call",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "last_call",
      label: "LTP",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "option_type_call",
      label: "Option Type",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "strike_price",
      label: "Strike Price",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "option_type_put",
      label: "Option Type",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "last_put",
      label: "LTP",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "iv_put",
      label: "IV Put",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "delta_put",
      label: "Delta Put",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "theta_put",
      label: "Theta Put",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "vega_put",
      label: "Vega Put",
      options: {
        filter: true,
        sort: true
      }
    },

    {
      name: "close_put",
      label: "Close Put",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },

    {
      name: "low_put",
      label: "low_put",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "high_put",
      label: "high_put",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "open_put",
      label: "open_put",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "number_of_contracts_put",
      label: "number_of_contracts_put",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "turnover_put",
      label: "Turnover Call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "premium_turnover_put",
      label: "Premium Turnover Call",
      options: {
        filter: true,
        sort: true,
        display: false
      }
    },
    {
      name: "oi_put",
      label: "OI Put",
      options: {
        filter: true,
        sort: true
      }
    },
    {
      name: "change_oi_put",
      label: "Change in OI Put",
      options: {
        filter: true,
        sort: true
      }
    }
  ];

  const options = {
    filterType: "checkbox",
    selectableRowsOnClick: "false"
  };

  return (
    <div className="OptChain">
      <Grid container justify="space-around">
        <FormControl
          variant="outlined"
          className={classes.formControl}
          inputProps={{ name: "symbol" }}
        >
          <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
            Select Underlying
          </InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={symbol}
            onChange={handleChange}
            labelWidth={labelWidth}
            inputProps={{ name: "symbol" }}
          >
            {fno.map((fno, index) => (
              <MenuItem
                inputProps={{ name: "symbol" }}
                key={index}
                value={fno}
                primaryText={fno}
                onChange={handleChange}
              >
                {fno}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            format="yyyy-MM-dd"
            margin="normal"
            variant="inline"
            name="edate"
            id="date-picker-inline"
            label="Select Expiry Date"
            value={edate}
            onChange={handleExDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date"
            }}
          />
          <KeyboardDatePicker
            margin="normal"
            variant="inline"
            id="date-picker-dialog"
            label="Select Snapshot Date"
            format="yyyy-MM-dd"
            name="sdate"
            value={sdate}
            maxDate={edate}
            maxDateMessage="Date should be on or before the expiry date"
            onChange={handleDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date"
            }}
          />
        </MuiPickersUtilsProvider>
        <FormControl variant="outlined" className={classes.formControl}>
          <Button
            variant="contained"
            className={classes.button}
            type="submit"
            value="submit"
            onClick={postData}
          >
            Submit
          </Button>
        </FormControl>
      </Grid>

      <MUIDataTable
        title={" Option Chain " + symbol}
        data={stockData}
        options={options}
        columns={columns}
      />
      <p>{stockData.iv_call}</p>
    </div>
  );
}

export default OptChain;
